const EXPRESS = require('express');
const APP = EXPRESS();
const PRODUCTS_MODEL = require('../models/products.js');

/**
* @route GET /products
* @group PRODUCTS
* @param {string} search.query.required- search keyword
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.get('/products',(req,res)=>{
    console.log(req.query)
    PRODUCTS_MODEL.search(req.query.search,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

module.exports = APP;