const EXPRESS = require('express');
const APP = EXPRESS();
const CART_MODEL = require('../models/cart.js');

/**
* @typedef CartPayload
* @property {string} owner_name.required - owner name
*/

/**
* @typedef CartItemsPayload
* @property {integer} product_id.required - product_id
* @property {number} quantity.required - quantity
*/


/**
* @route GET /cart
* @group CART
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.get('/cart',(req,res)=>{
    CART_MODEL.get(function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})


/**
* @route POST /cart
* @group CART
* @param {CartPayload.model} CartPayload.body - payload
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.post('/cart',(req,res)=>{
    CART_MODEL.create(req.body,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

/**
* @route DELETE /cart/{id}
* @group CART
* @param {integer} id.path.required - cart id
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.delete('/cart/:id',(req,res)=>{
    CART_MODEL.delete(req.params.id,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})


//==CART ITEMS==//
/**
* @route GET /cart/{cart_id}/items
* @group CART ITEMS
* @param {integer} cart_id.path.required - cart id
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.get('/cart/:cart_id/items',(req,res)=>{
    CART_MODEL.getItems(req.params.cart_id,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

/**
* @route POST /cart/{cart_id}/items
* @group CART ITEMS
* @param {integer} cart_id.path.required - cart id
* @param {CartItemsPayload.model} CartItemsPayload.body - payload
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.post('/cart/:cart_id/items',(req,res)=>{
    CART_MODEL.createItem(req.params.cart_id,req.body,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

/**
* @route DELETE /cart/{cart_id}/items/{item_id}
* @group CART ITEMS
* @param {integer} cart_id.path.required - cart id
* @param {integer} item_id.path.required - item id
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.delete('/cart/:cart_id/items/:item_id',(req,res)=>{
    CART_MODEL.deleteItem(req.params.item_id,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

module.exports = APP;