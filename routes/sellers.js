const EXPRESS = require('express');
const APP = EXPRESS();

const SELLERS_MODEL = require('../models/sellers.js');
const PRODUCTS_MODEL = require('../models/products.js');

/**
* @typedef SellerPayload
* @property {string} name.required - name
* @property {string} address.required - address
* @property {string} contact_number.required - contact number
*/

/**
* @typedef ProductPayload
* @property {string} name.required - name
* @property {string} description.required - address
* @property {number} price.required - contact number
*/

/**
* @route GET /sellers
* @group SELLERS
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.get('/sellers',(req,res)=>{
    SELLERS_MODEL.list(function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

/**
* @route GET /sellers/{id}
* @group SELLERS
* @param {integer} id.path.required - seller id
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.get('/sellers/:id',(req,res)=>{
    SELLERS_MODEL.read(req.params.id,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

/**
* @route POST /sellers
* @group SELLERS
* @param {SellerPayload.model} SellerPayload.body - payload
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.post('/sellers',(req,res)=>{
    console.log(req.body)
    SELLERS_MODEL.create(req.body,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})


/**
* @route PUT /sellers/{id}
* @group SELLERS
* @param {integer} id.path.required - seller id
* @param {SellerPayload.model} data.body - payload
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.put('/sellers/:id',(req,res)=>{
    SELLERS_MODEL.update(req.params.id,req.body,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

/**
* @route DELETE /sellers/{id}
* @group SELLERS
* @param {integer} id.path.required - seller id
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.delete('/sellers/:id',(req,res)=>{
    SELLERS_MODEL.delete(req.params.id,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})


//==SELLERS PRODUCTS==//

/**
* @route GET /sellers/{seller_id}/products
* @group SELLER PRODUCTS
* @param {integer} seller_id.path.required - seller id
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.get('/sellers/:seller_id/products',(req,res)=>{
    PRODUCTS_MODEL.listBySeller(req.params.seller_id,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})


/**
* @route POST /sellers/{seller_id}/products
* @group SELLER PRODUCTS
* @param {integer} seller_id.path.required - seller id
* @param {ProductPayload.model} ProductPayload.body - payload
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.post('/sellers/:seller_id/products',(req,res)=>{
    PRODUCTS_MODEL.create(req.params.seller_id,req.body,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})


/**
* @route PUT /sellers/{seller_id}/products/{product_id}
* @group SELLER PRODUCTS
* @param {integer} seller_id.path.required - seller id
* @param {integer} product_id.path.required - product id
* @param {ProductPayload.model} ProductPayload.body - payload
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.put('/sellers/:seller_id/products/:product_id',(req,res)=>{
    PRODUCTS_MODEL.update(req.params.seller_id,req.params.product_id,req.body,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

/**
* @route GET /sellers/{seller_id}/products/{product_id}
* @group SELLER PRODUCTS
* @param {integer} seller_id.path.required - seller id
* @param {integer} product_id.path.required - product id
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.get('/sellers/:seller_id/products/:product_id',(req,res)=>{
    PRODUCTS_MODEL.read(req.params.seller_id,req.params.product_id,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

/**
* @route DELETE /sellers/{seller_id}/products/{product_id}
* @group SELLER PRODUCTS
* @param {integer} seller_id.path.required - seller id
* @param {integer} product_id.path.required - product id
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.delete('/sellers/:seller_id/products/:product_id',(req,res)=>{
    PRODUCTS_MODEL.delete(req.params.seller_id,req.params.product_id,function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

module.exports = APP;