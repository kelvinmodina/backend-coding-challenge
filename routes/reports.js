const EXPRESS = require('express');
const APP = EXPRESS();
const REPORTS_MODEL = require('../models/reports.js');

/**
* @route GET /reports/products-cart-count
* @group REPORTS
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.get('/reports/products-cart-count',(req,res)=>{
    REPORTS_MODEL.productsCartCount(function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

/**
* @route GET /reports/top-sellers
* @group REPORTS
* @produces application/json
* @consumes application/json
* @returns 200 - An object of user info
*/
APP.get('/reports/top-sellers',(req,res)=>{
    REPORTS_MODEL.topSellers(function(err,result){
        res.json({
            message:err,
            status:(err ? 0 : 1),
            data: result
        });
    });
})

module.exports = APP;