
module.exports = function(APP){
    const expressSwagger = require('express-swagger-generator')(APP);

    let options = {
        swaggerDefinition: {
            info: {
                description: 'Coding Challenge documented API',
                title: 'Coding Challenge documented API',
                version: 1,
            },
            produces: [
                "application/json",
            ],
            schemes: ['http'],
            securityDefinitions: {
            }
        },
        basedir: process.env.Dirname, //app absolute path
        files: [process.env.Dirname + '/routes/*.js'] //Path to the API handle folder
    };
    expressSwagger(options);
}