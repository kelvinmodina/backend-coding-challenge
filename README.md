# Coding Challenge



## Project Specification

### Summary
- Provided API's for Seller, Seller Products, Cart, and Reports
- Used RESTful API formats to present functions to List/Create/Update/Delete

### How to Use
- Create a Seller first using the SELLER APIs. 
- Using the seller ID to create/assign a product using the SELLER PRODUCT API's 
- Create a cart using the CART APIs.
- Once you have a cart you can add items to the cart (CART ITEMS APIs) by using the product id and adding quantity.

## Installation

### Node Modules

Install prerequisite Node Modules
```bash
npm install
```
Install db-migrate module for database migration
```bash
npm i -g db-migrate
```

Install db-migrate-mysql module as an extension for mysql
```bash
npm i -g db-migrate-mysql
```
### Env Setup
- Copy .env.example and name it as .env
- Provide preferred App Details (URL & Port)
- Provide preferred Database details

### Database Setup
- navigate to 'db' directory 
```bash
cd db
```
- edit database.json and point to preferred database details
- run the migration script
```bash
db-migrate up
```
this will migrate all the database tables

## Usage
- Running the App
```bash
node app.js
```

- By default, the App will be running at http://localhost:3000
## Notes
- To test the API's I've included swagger documentation that can be accessed through http://localhost:3000/api-docs 

## Questions/Concerns
- if you have any questions or concerns kindly email me at [kelvinmodina@gmail.com](kelvinmodina@gmail.com)