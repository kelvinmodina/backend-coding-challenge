
function connect(){
    var mysql = require('mysql');
    var ENV = process.env;
    var _connectionString = {}
    
    _connectionString = {
        host: ENV.DB_HOST,
        user: ENV.DB_USERNAME,
        password: ENV.DB_PASSWORD,
        database: ENV.DB_DATABASE,
        connectTimeout: 60000,
        multipleStatements: true,
        timezone: 'utc'
    }

    
    var connection = mysql.createConnection(_connectionString);
    //console.log(_connectionString);

    return connection;
}

module.exports = {
    query: function(qry,param = [],callback){
        var con = connect();
        con.connect();
        var _query = con.query(qry, param, function (err, result) {
            con.destroy();

            callback(err,result);
        });
        console.debug(_query.sql);
    }
}
