'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  db.createTable('tbl_sellers', {
    ifNotExists: true,
    columns:{
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: 'string',
        length: 255
      },
      address: {
        type: 'text',
      },
      contact_number: {
        type: 'string',
        length: 255
      },
      date_created: {
        type: 'datetime'
      },
      status: {
        type: 'int'
      }
    }
  }, function(err) {
    console.log('tbl_sellers',err);
  });
  return null;
};

exports.down = function(db) {
  db.dropTable('tbl_sellers',
  {
    ifExists: true
  },function(err){
    console.log(err);
  })
  return null;
};

exports._meta = {
  "version": 1
};
