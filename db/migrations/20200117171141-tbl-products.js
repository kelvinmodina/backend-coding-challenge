'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  db.createTable('tbl_products', {
    ifNotExists: true,
    columns:{
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      seller_id: {
        type: 'int',
        foreignKey: {
          name: 'seller_id_fk',
          table: 'tbl_sellers',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id'
        }
      },
      name: {
        type: 'string',
        length: 255
      },
      description: {
        type: 'text'
      },
      price: {
        type: 'decimal',
        length:4
      },
      date_created: {
        type: 'datetime'
      },
      status: {
        type: 'int'
      }
    }
  }, function(err) {
    console.log('tbl_products',err);
    if(!err){
      db.runSql('ALTER TABLE tbl_products MODIFY COLUMN price decimal(11,4)',[],function(){
        
      });
    }
  });
  return null;
};

exports.down = function(db) {
  db.dropTable('tbl_products',
  {
    ifExists: true
  },function(err){
    console.log(err);
  })
  return null;
};

exports._meta = {
  "version": 1
};
