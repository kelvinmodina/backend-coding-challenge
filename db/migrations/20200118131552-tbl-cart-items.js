'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  db.createTable('tbl_cart_items', {
    ifNotExists: true,
    columns:{
      id: {
        type: 'int',
        primaryKey: true,
        autoIncrement: true
      },
      cart_id: {
        type: 'int',
        foreignKey: {
          name: 'cart_id_fk',
          table: 'tbl_carts',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id'
        }
      },
      product_id: {
        type: 'int',
        foreignKey: {
          name: 'seller_product_id_fk',
          table: 'tbl_products',
          rules: {
            onDelete: 'CASCADE',
            onUpdate: 'RESTRICT'
          },
          mapping: 'id'
        }
      },
      quantity: {
        type: 'int'
      },
      date_created: {
        type: 'datetime'
      },
      status: {
        type: 'int'
      }
    }
  }, function(err) {
    console.log('tbl_cart_items',err);
  });
  return null;
};

exports.down = function(db) {
  db.dropTable('tbl_cart_items',
  {
    ifExists: true
  },function(err){
    console.log(err);
  })
  return null;
};

exports._meta = {
  "version": 1
};
