const mysql = require('../helpers/mysqlConnector.js');

module.exports = {
    productsCartCount: function(callback){
        var _query = `
            SELECT
                tp.id,
                tp.name,
                tp.seller_id,
                COALESCE(SUM(tci.quantity),0) as addedToCart
            FROM
                tbl_products tp
            LEFT JOIN
                tbl_cart_items tci
            ON
                tp.id = tci.product_id
            GROUP BY
                tp.id
        `;
        mysql.query(_query, [], function (err, result) {
			callback(err, result);
		})
    },
    topSellers: function(callback){
        var _query = `
            SELECT
                ts.id,
                ts.name as seller_name,
                COALESCE(SUM(tci.quantity),0) as itemsSold
            FROM
                tbl_sellers ts
            LEFT JOIN
                tbl_products tp
            ON
                ts.id = tp.seller_id
            LEFT JOIN
                tbl_cart_items tci
            ON
                tp.id = tci.product_id
            GROUP BY
                ts.id
            ORDER BY
                itemsSold desc
        `;
        mysql.query(_query, [], function (err, result) {
			callback(err, result);
		})
    }
}