const mysql = require('../helpers/mysqlConnector.js');

module.exports = {
    list: function(callback){
        var _query = `
            SELECT
                *
            FROM
                tbl_sellers
            WHERE
                status = 1
        `;
        mysql.query(_query, [], function (err, result) {
			callback(err, result);
		})
    },
    create: function(data, callback){
        var _query = `
            INSERT
            INTO
                tbl_sellers
            SET
                ?,date_created=utc_timestamp(),status=1
        `;
        mysql.query(_query, [data], function (err, result) {
			callback(err, result);
		})
    },
    update: function(id, data, callback){
        var _query = `
            UPDATE
                tbl_sellers
            SET
                ?
            WHERE
                id=?
        `;
        mysql.query(_query, [data,id], function (err, result) {
			callback(err, result);
		})
    },
    delete: function(id,callback){
        var _query = `
            UPDATE
                tbl_sellers
            SET
                status = 0
            WHERE
                id=?
        `;
        mysql.query(_query, [id], function (err, result) {
			callback(err, result);
		})
    },
    read: function(id,callback){
        var _query = `
            SELECT
                *
            FROM
                tbl_sellers
            WHERE
                id=?
        `;
        mysql.query(_query, [id], function (err, result) {
			callback(err, (result && result[0] ? result[0] : {}));
		})
    }
}