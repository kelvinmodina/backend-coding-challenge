const mysql = require('../helpers/mysqlConnector.js');

module.exports = {
    get: function(callback){
        var _query = `
            SELECT
                *
            FROM
                tbl_carts 
            WHERE
                status = 1
        `;
        mysql.query(_query, [], function (err, result) {
			callback(err, result);
		})
    },
    create: function(data,callback){
        var _query = `
            INSERT
            INTO
                tbl_carts
            SET
                ?,date_created=utc_timestamp(),status=1
        `;
        mysql.query(_query, [data], function (err, result) {
			callback(err, result);
		})
    },
    delete: function(id,callback){
        var _query = `
            UPDATE
                tbl_carts
            SET
                status=0
            WHERE
                id=?
        `;
        mysql.query(_query, [id], function (err, result) {
			callback(err, result);
		})
    },
    getItems: function(cart_id,callback){
        var _query = `
            SELECT
                tci.id,
                tp.name,
                tp.description,
                tp.price,
                tci.quantity,
                tci.date_created
            FROM
                tbl_cart_items tci
            LEFT JOIN
                tbl_products tp
            ON
                tci.product_id = tp.id
            LEFT JOIN
                tbl_carts tc
            ON
                tci.cart_id = tc.id
            WHERE
                tc.id = ?
            AND
                tci.status = 1
        `;
        mysql.query(_query, [cart_id], function (err, result) {
			callback(err, result);
		})
    },
    createItem: function(cart_id,data,callback){
        data['cart_id'] = cart_id;
        var _query = `
            INSERT
            INTO
                tbl_cart_items
            SET
                ?,date_created=utc_timestamp(),status=1
        `;
        mysql.query(_query, [data], function (err, result) {
			callback(err, result);
		})
    },
    deleteItem: function(item_id,callback){
        var _query = `
            UPDATE
                tbl_cart_items
            SET
                status=0
            WHERE
                id=?
        `;
        mysql.query(_query, [item_id], function (err, result) {
			callback(err, result);
		})
    }
}