const mysql = require('../helpers/mysqlConnector.js');

module.exports = {
    listBySeller: function(seller_id,callback){
        var _query = `
            SELECT
                *
            FROM
                tbl_products
            WHERE
                seller_id = ?
            AND
                status = 1
        `;
        mysql.query(_query, [seller_id], function (err, result) {
			callback(err, result);
		})
    },
    read: function(seller_id,product_id,callback){
        var _query = `
            SELECT
                *
            FROM
                tbl_products
            WHERE
                seller_id = ? and id=?
        `;
        mysql.query(_query, [seller_id,product_id], function (err, result) {
			callback(err, result);
		})
    },
    create: function(seller_id,data, callback){
        var _query = `
            INSERT
            INTO
                tbl_products
            SET
                ?,seller_id=?,date_created=utc_timestamp(),status=1
        `;
        mysql.query(_query, [data,seller_id], function (err, result) {
			callback(err, result);
		})
    },
    update: function(seller_id,product_id, data, callback){
        var _query = `
            UPDATE
                tbl_products
            SET
                ?
            WHERE
                id=? and seller_id=?
        `;
        mysql.query(_query, [data,product_id,seller_id], function (err, result) {
			callback(err, result);
		})
    },
    delete: function(seller_id,product_id,callback){
        var _query = `
            UPDATE
                tbl_products
            SET
                status=0
            WHERE
                seller_id=? and id=?
        `;
        mysql.query(_query, [seller_id,product_id], function (err, result) {
			callback(err, result);
		})
    },
    search:function(search_query,callback){
        search_query = `%${search_query}%`;
        var _query = `
            SELECT 
                *
            FROM
                tbl_products
            WHERE
                (name like ?
            OR
                description like ?)
            AND
                status = 1
        `;
        mysql.query(_query, [search_query,search_query], function (err, result) {
			callback(err, result);
		})
    }
}