require('dotenv').config(); //used to fetch .env and render as process.env
process.env.Dirname = __dirname;

const EXPRESS = require('express');
const APP = EXPRESS();
const PORT = process.env.APP_PORT;
const SERVER = require('http').createServer(APP);
const BODY_PARSER = require('body-parser');

var cors = require('cors')
APP.options('*', cors());

APP.use(BODY_PARSER.json({strict: false}));
APP.use(BODY_PARSER.urlencoded({extended: true}));
        
const SELLERS_ROUTE = require('./routes/sellers.js');
const PRODUCTS_ROUTE = require('./routes/products.js');
const CART_ROUTE = require('./routes/cart.js');
const REPORTS_ROUTE = require('./routes/reports.js');

// route for /services or /services/*
APP.all('/sellers(/*)?',SELLERS_ROUTE);
APP.all('/products(/*)?',PRODUCTS_ROUTE);
APP.all('/cart(/*)?',CART_ROUTE);
APP.all('/reports(/*)?',REPORTS_ROUTE);

require('./documentation/swagger.js')(APP);

SERVER.listen(PORT, () => console.log(`Server PORT: ${PORT}!`));